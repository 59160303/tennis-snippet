test('"Love - Love" when start the game', () => {
    var game = new TennisGame()
    game.start()

    expect(game.echoScore()).toBe('Love - Love')
})

test('"Fifteen - Love" when PlayerA get first score', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('Fifteen - Love')
})

test('"Thirty - Love" when PlayerA get double score', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('Thirty - Love')
})

test('"Fourty - Love" when PlayerA get tripple score', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()
    game.playerAGetScore()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('Fourty - Love')
})

test('"PlayerA wins the game" when PlayerA get 4 score before PlayerB', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()
    game.playerAGetScore()
    game.playerAGetScore()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('PlayerA wins the game')
})

test('"Love - Fifteen" when PlayerB get first score', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('Love - Fifteen')
})

test('"Love - Thirty" when PlayerB get double score', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('Love - Thirty')
})

test('"Love - Fourty" when PlayerB get tripple score', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()
    game.playerBGetScore()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('Love - Fourty')
})

test('"PlayerB wins the game" when PlayerB get 4 scores before PlayerA', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()
    game.playerBGetScore()
    game.playerBGetScore()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('PlayerB wins the game')
})
